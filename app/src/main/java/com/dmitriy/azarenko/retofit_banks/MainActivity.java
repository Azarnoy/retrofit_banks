package com.dmitriy.azarenko.retofit_banks;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {
    TextView eur;
    TextView usd;
    TextView rub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eur = (TextView)findViewById(R.id.pb_uer_id);
        usd = (TextView)findViewById(R.id.pb_usd_id);
        rub = (TextView)findViewById(R.id.pb_rub_id);

        Retrofit.getValue(new Callback<List<Banks>>() {
            @Override
            public void success(List<Banks> bankses, Response response) {
                String eurName = bankses.get(0).EUR;
                eur.setText(eurName);


            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "SOmthing Wrong", Toast.LENGTH_SHORT).show();

            }
        });



    }
}
