package com.dmitriy.azarenko.retofit_banks;

/**
 * Created by Дмитрий on 15.02.2016.
 */
public class Banks {

    String EUR;
    String RUB;
    String USD;

    public String getEUR() {
        return EUR;
    }

    public void setEUR(String EUR) {
        this.EUR = EUR;
    }

    public String getRUB() {
        return RUB;
    }

    public void setRUB(String RUB) {
        this.RUB = RUB;
    }

    public String getUSD() {
        return USD;
    }

    public void setUSD(String USD) {
        this.USD = USD;
    }
}
