package com.dmitriy.azarenko.retofit_banks;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

/**
 * Created by Дмитрий on 15.02.2016.
 */
public class Retrofit {

        private static final String ENDPOINT = "http://http://resources.finance.ua/ru/public";
        private static ApiInterface apiInterface;

        interface ApiInterface {
            @GET("/currency-cash.json")
            void getValue(Callback<List<Banks>> callback);


        }

        static {
            init();
        }

        private static void init() {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(ENDPOINT)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            apiInterface = restAdapter.create(ApiInterface.class);
        }

        public static void getValue(Callback<List<Banks>> callback) {
            apiInterface.getValue(callback);
        }



}



